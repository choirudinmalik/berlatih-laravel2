@extends('master');

@section('content')
			<div class="panel-heading ml-4 mb-2">
                <a href="/pertanyaan" title="Input data"><button name="input" class="btn btn-warning">Kembali</button></a>
            </div>
			
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Detail Pertanyaan
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Judul</dt>
                  <dd>{{$data -> judul}}</dd>
                  <dt>Isi</dt>
                  <dd>{{$data -> isi}}</dd>
                  <dt>Tanggal Dibuat</dt>
                  <dd>{{$data -> tanggal_dibuat}}</dd>
				  <dt>Tanggal Diperbaharui</dt>
                  <dd>{{$data -> tanggal_diperbaharui}}</dd>
				  <dt>Jawaban Terbaik</dt>
                  <dd>{{$data -> jawaban_tepat_id}}</dd>
				  <dt>Pembuat Pertanyaan</dt>
                  <dd>{{$data -> profil_id}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
@endsection