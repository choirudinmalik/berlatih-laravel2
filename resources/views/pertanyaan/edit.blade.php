@extends('master');

@section('content')
		<div class="panel-heading ml-4 mb-2">
                <a href="/pertanyaan" title="Input data"><button name="input" class="btn btn-warning">Kembali</button></a>
        </div>
			
		<div class="ml-4">
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$data -> id}}" method="POST">
			  @csrf
			  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $data -> judul)}}" placeholder="Tuliskan Judul Pertanyaan Anda">
					@error('judul')
						<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				  </div>
                  <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $data -> isi)}}" placeholder="Tuliskan Isi Pertanyaan Anda">
					@error('isi')
						<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update Pertanyaan</button>
                </div>
              </form>
            </div>
		</div>
@endsection