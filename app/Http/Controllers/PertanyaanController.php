<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
		$data = DB::table('pertanyaan')->get();
		return view('pertanyaan.index', compact('data'));
	}
	
	public function create(){
		return view('pertanyaan.create');
	}
	
	public function store(Request $request){
		
		$request->validate([
			'judul' => ['required', 'max:45'],
			'isi' => ['required', 'max:255']
		]);
		
		$sql = DB::table('pertanyaan')->insert([
			'judul' => $request['judul'], 
			'isi' => $request['isi'],
			'tanggal_dibuat' => now(),
			'tanggal_diperbaharui' => now()
		]);
		return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dikirim!');
	}
	
	public function show($id){
		$data = DB::table('pertanyaan')->where('id', $id)->first();
		return view('pertanyaan.show', compact('data'));
	}
	
	public function edit($id){
		$data = DB::table('pertanyaan')->where('id', $id)->first();
		return view('pertanyaan.edit', compact('data'));
	}
	
	public function update($id, Request $request){
		$request->validate([
			'judul' => ['required', 'max:45'],
			'isi' => ['required', 'max:255']
		]);
		
		$sql = DB::table('pertanyaan')
				->where('id', $id)
				->update([
					'judul' => $request['judul'], 
					'isi' => $request['isi'],
					'tanggal_dibuat' => now(),
					'tanggal_diperbaharui' => now()
				]);
		return redirect('/pertanyaan')->with('success', 'Update Pertanyaan Berhasil!');
	}
	
	public function destroy($id){
		$sql = DB::table('pertanyaan')->where('id', $id)->delete();
		return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
	}
}
